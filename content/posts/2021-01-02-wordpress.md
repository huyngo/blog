---
category: blog
date:   2021-01-02 14:44:00 +0700
lang: en
translationKey: "give-up-wp"
tags: [blog, wordpress, writing, selfhost]
title:  "Giving up on WordPress!"
---
For the last week, I was trying to setting up my own WordPress instance, since I have access to a server and use a free domain name. It was such a hassle. <!--more-->

## Why I want to start a blog

Writing is a way of polishing my writing skills. For so long, all what I write has been only code. Now that I'm close to my graduation, writing long text suddenly becomes important to me - I need to write reports and after that my thesis.

It is also a way of expressing myself. Social media is a shorter version for this, and they may attract more interactions from people you know, but blog on the other hand helps you express your idea and opinion publicly. They're also much more organized in the sense that my list of posts does not contain shares from other users, or that I can use hashtags to categorize my posts.

## Choosing WordPress

At first I was thinking of using wordpress.com, since it's already hosted, but then I realized they can put ads on my blogs without my consent (well, technically, they would have my consent if I used their service). On top of that, I always prefer self-hosting whenever possible: I have all control (doubt) over what is running and what not. I would not what a third party to track my readers or showing them ads on my blog.

The server was running on Ubuntu 16.04. That is a rather old one, so it doesn't support latest packages, including Apache, PHP, and WordPress (they do have WordPress in the Ubuntu repo), and I dare not upgrade it.

On the other hand, I already hosted some other services with Nginx there, so I'd prefer to use Nginx instead of Apache. They do have [that option](https://wordpress.org/support/article/nginx/), but alas, those configs file seem long and I was not sure if I would break something while editing the nginx.conf file.

I did try to use Nginx as a reverse proxy to my Apache server, though. It did work, but there is another problem, which I will talk about in a next section

## Using Docker

I also tried another option - docker. I wondered why I didn't think of it earlier.

I have recently been using docker quite regularly, e.g. for CouchDB and RethinkDB which aren't packaged for my distro, or SQLServer and MongoDB, which were required for my database course. Docker is a way to easily run some program in a contained environment with simple configuration that can be input from command line or docker-compose file. Most importantly, they're confirmed to work.

And nice, it did run.

Now I need to set up a domain name for the blog.

## Port forwarding

For personal reason, I'd rather not reveal my domain name here, so I'd use `example.com` in its place.

I signed up a domain name at freenom, and it only took me one day until the domain name is fully available.

The only problem is that `example.com` always show me a bad gateway response. I made sure my Nginx configuration were right, but somehow it weren't listening  on port 80 - the default port for HTTP. I had to go to `example.com:12345` (the port I ran my docker on), and for normal users that doesn't look intuitive at all.

It turned out, I didn't link my configuration correctly, so there are broken symlinks in `/etc/nginx/sites-enabled/` and the configuration was never included.

After I fixed that, it worked nicely.

## Different name == broken website

WordPress uses absolute path (`example.com/blog/something`) rather than relative path (`/blog/something`). This leads to a problem: if I want to migrate to another domain name (guaranteed to happen, since I use a free domain name that will expire eventually). I discovered this when I moved from a port to different port, and from host IP to domain name. It frustrates me, but it isn't a big problem for now.

## Disappointing experience

I did successfully hosted my own WP instance, but it didn't feel right for me.

WordPress is so bloated. They target non-programmer, and they have a lot of drag-and-drop editing. It is hard for me to select the right block and edit it. They have a lot of features for images and widgets that I won't use. Of course I can remove those features, but they would take time and not is worth it.

As a result, I'm refrained from posting there.

## Write.as

Then I was recommended this page on [Mastodon](https://joinmastodon.org/). It feels so right to me: as a programmer I love writing with Markdown - the markup language is so easy to write, and it allows me to write quickly. I can also write it in a preferred text editor such as vim and paste the text here.

I did try to host my own [WriteFreely](https://writefreely.org/) instance (the technology behind write.as), but since I invested too much energy for WordPress I don't feel like it now, even though it seems simple according to the [installation instruction](https://writefreely.org/start). The [docker image](https://hub.docker.com/r/writeas/writefreely) is also not at all documented.

Fortunately, write.as already feels right to me - no ads, and tracking is easily opted out. The only problem here is that my domain name is wasted there.

## Update

Well, I just installed (it was actually easy!), and now I've moved to divers.ml

New update: I just tried jekyll, and it's awesome. Now my blog is hosted on [GitHub](https://huy-ngo.github.io). I guess I will leave the domain unused.
