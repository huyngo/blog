---
category: blog
date:   2021-01-02 14:44:00 +0700
lang: fr
tags: [blog, wordpress, writing, selfhost]
title:  "Je quitte WordPress"
translationKey: "give-up-wp"
---

Pendant la semaine dernière, j'installe à peine ma propre instance de WordPress,
parce que j'ai accès à un serveur et je peux utiliser un nom de domaine gratuit.
Mais c'était quel horreur. <!--more-->

# Pourquoir écrire un blog

J'écris pour le faire mieux (et pour me défier de écrire des articles en langues
à part anglais). Il y avait longtemps que je n'ai rien écrit que le code.
Mais écrire des essais c'est important pour moi : pour ma graduation, il faut écrire
des rapports et un thèse.

Mais ce n'est pas la seule raison. C'est aussi un moyen de m'exprimer. Le média sociel
peut m'aider à le faire, mais je quitte Facebook à cause du manque de confidentialité
premièrement et de la haine partout où je cherche le divertissement, et Mastodon a une
limitation de caractères pour une publication. Les catégories et les tags sont aussi
des aides pour mettre des publications en ordre.

# Choisir WordPress

WordPress c'est une plate-forme populaire pour blogger. Au debut je pensais de choisir
wordpress.com, car il est déjà hébergé. Mais j'ai découvert qu'il mettrais des ads
sur mes blogs sans mon approbation. De plus, si je héberge moi-même, j'ai du contrôle.
Je ne veux pas que mes lecteurs doivent voir les ads qui sont mis par un tier.

Le serveur fonctionne sous Ubuntu 16.04, qui est très vieux. Ainsi, il n'a pas les plus
récents paquets, y compris Apache, PHP, et WordPress, et je n'ose pas le mettre à niveau.

J'y avait déjà des autres services avec nginx, alors je préfère utiliser nginx comme
serveur web. Il y a [un guide][wp-nginx] pour exactement ça, mais malheureusement,
ces fichiers de la configuration sont longs et j'ai peur de mettre quelque chose en panne
cependant (et de plus, je suis paresseux ;) ).

Alors, c'est ça, j'utilise Apache, ce n'est pas une mauvaise chose, et j'utilise nginx
comme proxy inverse. Ça a marché.

# Utiliser Docker

J'ai aussi essaié docker. Je me demandais pourquoi je n'avais pas penser à
cette solution plus tôt.
J'ai récemment utilisé docker très fréquemment, e.g. pour CouchDB et RethinkDB, qui
ne sont pas paqueté pour Tumbleweed, our SQLServer et MongoDB, qui étaient nécessaire
pour mon cours de base de données.

Docker exécute des programmes dans un container ( « conteneur » ) que 
l'on peut facilement configurer avec docker-compose ou par ligne de commande.
Surtout, on peut être sûr que ça marche.

Et ça marche merveilleusement.

# Redirection de port

Ce n'était pas un problème que WordPress a créé, c'était ma faute, mais comme la faute
est aussi une leçon, je la raconte quand même.

Pour ne pas faire la même faute à l'avenir: il faut lier les fichiers configs dans
`/etc/nginx/sites-available/` à `/etc/nginx/sites-enabled/` avec des paths absolus,
sinon le lien serait cassé et ne marcherait pas.

# Changer le nom casse les liens

WordPress utilise apparamment des liens absolus (`example.com/blog/quelque-chose`)
au lieu des liens relatifs (`/blog/quelque-chose`).
C'est pas un bon pratique : si je change le hébergeur ou le nom de domain
(qui arrivera sûrement, car les noms de domaine gratuits expiront tôt ou tard).
Je l'ai découvert quand je changeais de IP du hébergeur au nom de domaine.

# Des expériences déçues

WordPress n'est pas la chose pour moi. Il contient trop de drag-and-drop pour écrire
un post que je trouve difficile à utiliser. Il contient aussi beaucoup de widgets que
je ne jamais utiliser et qui prend trop de temps à charger.

Étant résultat, je n'ai pas envie d'y écrire.

# Write.as

Et puis quelqu'un sur Mastodon m'a fait savoir de write.as.
Cette service utilise Markdown pour rendre les posts, et comme Markdown est si facile
à écrire, je l'aimais immédiatement.

[WriteFreely][writefreely] est le logiciel grâce auquel write.as fonctionne. J'ai
hébergé une instance moi-même, et il marchait exactement comme j'en ai attendu.

# Mise à jour

J'ai essayé [jekyll][jekyll], et c'est merveilleux.

Maintenant mon blog est sur [GitHub](https://huy-ngo.github.io)
et [huyngo.cf](http://huyngo.cf). GitHub ne supporte pas des thèmes que j'utilise, alors
là le blog ne apparaît correctement.

Jekyll est meilleur que WriteFreely pour moi, parce que les posts sont stockés simplement
comme texte plain, par contre WriteFreely les stocke dans SQL, ce que je considère
un overhead. Le seul désavantage c'est que je n'ai jamais appris Ruby avant.

J'ai envie d'essayer [Hugo][hugo], qui est programmé dans Go, une langue que je connais
mieux que Ruby. En outre, Hugo supporte rendre des pages pour gemini, que je veux aussi
essayer.
Mais ça va attendre, je ne devrais pas dépenser trop de temps pour
la choice de techonologie.

[wp-nginx]: https://wordpress.org/support/article/nginx/
[writefreely]: https://github.com/writeas/writefreely
[jekyll]: https://jekyllrb.com/
[hugo]: https://gohugo.io
