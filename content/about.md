---
title: About
language: en
ref: about
menu:
  about:
    identifier: about
    name: About
    url: /about/
    weight: 1
  social:
    identifier: social
    parent: about
    name: Social media
    url: /about/social/
    weight: 2
  works:
    identifier: works
    parent: about
    name: Works
    url: /about/works/
    weight: 3
---

I am Huy. Use the pronoun "he" to refer to me in third person.  I'm from
Vietnam and besides Vietnamese and English; I also speak some French and
German.  I write mainly in English, since that's the one I'm most comfortable
with, but I will translate some of my posts to other languages for practice.

I write about:

- Programming
- Learning languages
- Conlanging
- Digital freedom
- Books or fiction in general

But it's primarily about technology in general.

# Correct mistakes

Any correction is appreciated, whether I state a wrong fact or make a grammar mistake.

# Copying

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0"
     src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
All content in this blog is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>,
unless stated otherwise.

This means you are free to share the posts and modify them, provided that you
attribute properly, link to the original content, and state your changes.
Additionally, you have to share those changes under the same license.

# Donate

I receive donate on [liberapay/huy.ngo][liberapay] and [patreon/\_\_huy_ngo\_\_][patreon].

# Source

The source code for generating this website can be found on [source hut][srht-blog].

[cv]: https://raw.githubusercontent.com/Huy-Ngo/my-cv/master/huy-cv.pdf
[jekyll]: https://github.com/jekyll/jekyll
[minima]: https://github.com/jekyll/minima
[openring]: https://git.sr.ht/~sircmpwn/openring
[srht-blog]: https://git.sr.ht/~huyngo/blog
[liberapay]: https://liberapay.com/huy.ngo/
[patreon]: https://www.patreon.com/__huy_ngo__
