---
title: Work
language: en
ref: works
slug: /about/works/
menu:
  about:
    name: About
    url: /about/
    weight: 1
  social:
    parent: about
    name: Social media
    url: /about/social/
    weight: 2
  works:
    parent: about
    name: Works
    url: /about/works/
    weight: 3
---

I am a web backend developer, though I'm open to experiment in other fields as
well.  You can look at my project on:

- [GitHub](https://github.com/Huy-Ngo/)
- [Disroot Git](https://git.disroot.org/huyngo)
- [source hut](sr.ht/~huyngo/)

See my CV in PDF format [here][cv].

# Education

- Bachelor: University of Science and Technology of Hanoi, 2018-2021

# Work Experience

## Internship

So far I have only worked as intern in VC Corp. During my internship there, I
created a disposable mail system as a proof of concept. Afterwards, I have been
working with their mail system for a short while.

# Projects

## School projects

### Acanban

[Acanban][acanban] is an in-development academic-oriented project management
system.

So far, we have implemented only basic project collaboration features. There is
much work to be done in the future.

### Palace

[Palace][palace] is a 3D audio library in Python.  It is a wrapper around
OpenAL for Python, expecting to provide developers with modern and intuitive
utilities for audio processing.

## Personal projects

- [discord-meme-bot], a meme bot for discord
- [wikt-cli], a CLI tool to search Wiktionary using its API
- [cov-news][cov], a crawler for COVID-19 related news 
- [GenWord][genword] a word generator on Android, alternative to [its JS equivalent][gen]

## Miscellaneous

- [YAltaCV], a Python script that generate AltaCV-themed CV from YAML.
- [Round Robin][rr-sched], a web-based personal time scheduler inspired by
  process scheduler algorithm of the same name.

# Languages

I'm most comfortable with Vietnamese for casual conversation and English for
working (such as for documentation and commit message). I also can speak some
French and German with low fluency and read/write them with the help of a
dictionary.

[cv]: https://raw.githubusercontent.com/Huy-Ngo/my-cv/master/huy-cv.pdf
[palace]: https://git.sr.ht/~cnx/palace
[acanban]: https://github.com/Huy-Ngo/acanban
[YAltaCV]: https://github.com/Huy-Ngo/YAltaCV
[discord-meme-bot]: https://github.com/Huy-Ngo/discord-meme-bot
[wikt-cli]: https://git.sr.ht/~huyngo/wikt-cli
[genword]: https://git.sr.ht/~huyngo/GenWord
[cov]: https://github.com/Huy-Ngo/cov-news
[gen]: www.zompist.com/gen.html
