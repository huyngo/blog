---
title: Social Media
language: en
ref: social
slug: /about/social/
menu:
  about:
    name: About
    url: /about/
    weight: 1
  social:
    parent: about
    name: Social media
    url: /about/social/
    weight: 2
  works:
    parent: about
    name: Works
    url: /about/works/
    weight: 3
---

I practically left Facebook et al. Currently I am federated social networks, namely:

- Mastodon: For short text post, but it's longer than Twitter
- Pixelfed: Federated Instagram, where I post memes
- Matrix.org: Instant messaging, with E2EE for private messages

You can find my Mastodon account at the footer and in my CV. Ask me further on
Mastodon or email if you really want to know other two.
