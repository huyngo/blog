# Blog

This website is built with Jekyll. The code is released under [MIT] License.
The content is released under a [Creative Commons Attribution-ShareAlike 4.0 International][CC-BY-SA-4.0] license.

[MIT]: https://opensource.org/licenses/MIT
[CC-BY-SA-4.0]: https://creativecommons.org/licenses/by-sa/4.0/legalcode
