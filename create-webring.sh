# Create webring
openring \
  -s https://blog.golang.org/feed.atom \
  -s https://drewdevault.com/feed.xml \
  -s https://news.opensuse.org/feed/ \
  -s https://stallman.org/rss/rss.xml \
  -s https://www.eff.org/rss/updates.xml \
  < webring.template \
  > layouts/partials/custom-webring.html
